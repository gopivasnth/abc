package com.autoBot.pages;



import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.testleaf.leaftaps.base.ProjectBase;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations{
	public MyLeadsPage() {
		PageFactory.initElements(driver, this);


}
	@CacheLookup
	@FindBy(how=How.LINK_TEXT, using="Create Lead") 
	WebElement eleCreateLead;
	
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	
	/*public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadPage();
	}
	
	public FindLeadsPage clickFindLead()
	{
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadsPage();
	}*/

}