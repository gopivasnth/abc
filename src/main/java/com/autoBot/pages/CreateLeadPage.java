package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

//import org.testleaf.leaftaps.base.ProjectBase;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{
	public CreateLeadPage() {
	


	
		PageFactory.initElements(driver, this);

}
	@CacheLookup
	@FindBy(how=How.ID, using="createLeadForm_companyName") 
	WebElement eleCompanyNames;
	@FindBy(how=How.ID, using="createLeadForm_firstName") 
	WebElement eleFirstNames;
	@FindBy(how=How.ID, using="createLeadForm_lastName") 
	WebElement eleLastNames;
	/*public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
		return this;
	}*/
	public CreateLeadPage enterCompanyName(String data) {
		//driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
		clearAndType(eleCompanyNames, data);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFirstNames, data);
		return this;
	}
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastNames, data);	
		return this;
	}
	
	/*public ViewLeadPage clickCreateButton() {
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();
		
//		return new HomePage();
	}*/

}

